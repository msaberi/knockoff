import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Ridge

#fisher
data = pd.read_csv("model_x_fin.csv")
yy = pd.read_csv("model_y.csv")
# print(yy)
yy = yy.transpose()
y = yy.iloc[0]
y = y.values
# y = list(y)
# print("y" , type(y.iloc[0]))
t_data  = data.transpose()
# t_data = data
td_data = pd.DataFrame(t_data)
td_data = td_data.values
print(t_data)
print("x",type(td_data))
# oddsratio, pvalue = stats.fisher_exact(t_data)


#pearson
#Using Pearson Correlation
# print("1")
# plt.figure(figsize=(12,10))
# print("2")
# cor = t_data.corr()
# print("3")
# sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
# print("4")
# plt.show()

#Loading the dataset
# x = load_boston()
# df = pd.DataFrame(x.data, columns = x.feature_names)
# df["MEDV"] = x.target
# X = df.drop("MEDV",1)   #Feature Matrix
# y = df["MEDV"]          #Target Variable
# print(type(X))
# print(type(y))
# df.head()

# #Adding constant column of ones, mandatory for sm.OLS model
print("y" , type(y), y )
# X_1 = sm.add_constant(td_data)
#Fitting sm.OLS model
# model = sm.OLS(y,td_data).fit()
# print(model.pvalues)
#
# #Backward Elimination
# cols = list(td_data.columns)
# pmax = 1
# while (len(cols)>0):
#     p= []
#     X_1 = td_data[cols]
#     X_1 = sm.add_constant(X_1)
#     model = sm.OLS(y,X_1).fit()
#     p = pd.Series(model.pvalues.values[1:],index = cols)
#     pmax = max(p)
#     feature_with_p_max = p.idxmax()
#     if(pmax>0.05):
#         cols.remove(feature_with_p_max)
#     else:
#         break
# selected_features_BE = cols
# print(selected_features_BE)


test = SelectKBest(score_func=chi2, k=4)
fit = test.fit(td_data, y)

# Summarize scores
np.set_printoptions(precision=3)
print(fit.scores_)

features = fit.transform(td_data)
# Summarize selected features
print(features[0:5,:])

model = LogisticRegression()

#model = LogisticRegression(solver='liblinear')
# create and configure model
#model = LogisticRegression(solver='lbfgs', multi_class='auto')
rfe = RFE(model, 3)
fit = rfe.fit(td_data, y)
print("Num Features: %s" % (fit.n_features_))
print("Selected Features: %s" % (fit.support_))
print("Feature Ranking: %s" % (fit.ranking_))
ridge = Ridge(alpha=1.0)
ridge.fit(td_data,y)
# A helper method for pretty-printing the coefficients
def pretty_print_coefs(coefs, names = None, sort = False):
    if names == None:
        names = ["X%s" % x for x in range(len(coefs))]
    lst = zip(coefs, names)
    if sort:
        lst = sorted(lst,  key = lambda x:-np.abs(x[0]))
    return " + ".join("%s * %s" % (round(coef, 3), name)
                                   for coef, name in lst)

print ("Ridge model:", pretty_print_coefs(ridge.coef_))

