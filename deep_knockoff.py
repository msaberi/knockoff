import numpy as np
from DeepKnockoffs import KnockoffMachine
from DeepKnockoffs import GaussianKnockoffs
import data
import parameters
import matplotlib.pyplot as plt

X_train= np.genfromtxt('xx.csv', delimiter=',')
# print(X_train)
remove_col=[]
for i in range(len(X_train)):
    sum=0
    for j in range(len(X_train[i])):
        if X_train[i][j]==0:
            sum+=1

    if sum/len(X_train[i])>0.8:
        remove_col.append(i)
X_train=np.transpose(X_train)
X_train = np.delete(X_train, remove_col, axis=1)
print((X_train).shape)
model='unknown'

# Measure pairwise second-order knockoff correlations
corr_g = np.array([4]*X_train.shape[1])
hyper=parameters.GetTrainingHyperParams("sparse")
# print('Average absolute pairwise correlation: %.3f.' % (np.mean(np.abs(corr_g))))

p = (X_train).shape[1]  # feature
n = (X_train).shape[0]  # sample

pars = dict()
# Number of epochs
pars['epochs'] = 100
# Number of iterations over the full data per epoch
pars['epoch_length'] = 100
# Data type, either "continuous" or "binary"
pars['family'] = "continuous"
# Dimensions of the data
pars['p'] = p
# Size of the test set
pars['test_size'] = 0
# Batch size
pars['batch_size'] = int(0.5 * n)
# Learning rate
pars['lr'] = 0.000001
# When to decrease learning rate (unuswed when equal to number of epochs)
pars['lr_milestones'] = [pars['epochs']]
# Width of the network (number of layers is fixed to 6)
pars['dim_h'] = int(p)
# Penalty for the MMD distance
pars['GAMMA'] = 0.5
# Penalty encouraging second-order knockoffs
pars['LAMBDA'] = 0.1
# Decorrelation penalty hyperparameter
pars['DELTA'] = 0.5
# Target pairwise correlations between variables and knockoffs
pars['target_corr'] = corr_g
# Kernel widths for the MMD measure (uniform weights)
pars['alphas'] = [1., 2., 4., 8., 16., 32., 64., 128.]

# Where to store the machine
checkpoint_name = "tmp/" + model

# Where to print progress information
logs_name = "tmp/" + model + "_progress.txt"
machine = KnockoffMachine(pars, checkpoint_name=checkpoint_name,logs_name=logs_name)
# Train the machine
print("Fitting the knockoff machine...")
machine.load(checkpoint_name)
Xk_train_m = machine.generate(X_train)
np.savetxt("Xk_train_m.csv", Xk_train_m, delimiter=",")
# print(Xk_train_m.shape)
print("Size of the deep knockoff dataset: %d x %d." %(Xk_train_m.shape))

def PlotScatterHelper(A, B, ax=None):
    """
    Plot the entries of a matrix A vs those of B
    :param A: n-by-p data matrix
    :param B: n-by-p data matrix
    """
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = None

    ax.set_xlim([-0.2, 1.1])
    ax.set_ylim([-0.2, 1.1])

    for i in range(0, A.shape[0] - 1):
        ax.scatter(A.diagonal(i), B.diagonal(i), alpha=0.2)

    lims = [
        np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
        np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    ]

    for i in range(0, A.shape[0] - 1):
        meanValA = np.mean(A.diagonal(i))
        meanValB = np.mean(B.diagonal(i))
        ax.plot([meanValA, meanValA], lims, 'k-', alpha=0.2, zorder=0)
        if i == 0:
            color = 'r-'
            alpha = 1
        else:
            color = 'k-'
            alpha = 0.2
        ax.plot(lims, [meanValB, meanValB], color, alpha=alpha, zorder=0)

    # Plot both limits against each other
    ax.plot(lims, lims, 'k-', dashes=[2, 2], alpha=0.75, zorder=0)
    ax.set_aspect('equal')
    return ax


def ScatterCovariance(X, Xk):
    """ Plot the entries of Cov(Xk,Xk) vs Cov(X,X) and Cov(X,Xk) vs Cov(X,X)
    :param X: n-by-p matrix of original variables
    :param Xk: n-by-p matrix of knockoffs
    """
    # Create subplots
    fig, axarr = plt.subplots(1, 2, figsize=(14, 7))

    # Originality
    XX = np.corrcoef(X.T)
    XkXk = np.corrcoef(Xk.T)
    PlotScatterHelper(XX, XkXk, ax=axarr[0])
    axarr[0].set_xlabel(r'$\hat{G}_{\mathbf{X}\mathbf{X}}(i,j)$')
    axarr[0].set_ylabel(r'$\hat{G}_{\tilde{\mathbf{X}}\tilde{\mathbf{X}}}(i,j)$')

    # Exchangeability
    p = X.shape[1]
    G = np.corrcoef(X.T, Xk.T)
    XX = G[:p, :p]
    XXk = G[:p, p:(2 * p)]
    PlotScatterHelper(XX, XXk, ax=axarr[1])
    axarr[1].set_xlabel(r'$\hat{G}_{\mathbf{X}\mathbf{X}}(i,j)$')
    axarr[1].set_ylabel(r'$\hat{G}_{\mathbf{X}\tilde{\mathbf{X}}}(i,j)$')

    return fig


#ScatterCovariance(X_train, Xk_train_m).savefig('compare.png'